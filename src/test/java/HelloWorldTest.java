import org.junit.Assert;
import org.junit.Test;

public class HelloWorldTest {

    public String expected = "Hello World!";

    @Test
    public void firstTest() {
         HelloWorld helloWorld = new HelloWorld();
         String result = helloWorld.helloWorld();
         Assert.assertEquals(expected, result);
    }
}
